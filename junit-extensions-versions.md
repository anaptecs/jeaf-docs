# JEAF JUnit Extensions Latest Versions

|                         |                           |                         |                           |                           |
|-------------------------|:-------------------------:|-------------------------|:-------------------------:|:-------------------------:|
| JEAF JUnit Extensions |![Current Version](https://maven-badges.herokuapp.com/maven-central/com.anaptecs.jeaf.junit/jeaf-junit-extensions/badge.svg) | [Javadoc](https://javadoc.io/doc/com.anaptecs.jeaf.junit/jeaf-junit-extensions/latest/index.html) <br><br> | [Maven Repo](https://search.maven.org/artifact/com.anaptecs.jeaf.junit/jeaf-junit-extensions) | [Git Repo](https://bitbucket.org/anaptecs/jeaf-junit-extensions-impl/) |
