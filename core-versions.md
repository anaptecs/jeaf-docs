# JEAF Core Latest Versions

|                         |                           |                         |                           |                           |
|-------------------------|:-------------------------:|-------------------------|:-------------------------:|:-------------------------:|
| JEAF Core API |![Current Version](https://maven-badges.herokuapp.com/maven-central/com.anaptecs.jeaf.core/jeaf-core-api/badge.svg) | [Javadoc](https://javadoc.io/doc/com.anaptecs.jeaf.core/jeaf-core-api/latest/index.html) <br><br> | [Maven Repo](https://search.maven.org/artifact/com.anaptecs.jeaf.core/jeaf-core-api) | [Git Repo](https://bitbucket.org/anaptecs/jeaf-core-api/) |
| JEAF Core Implementation |![Current Version](https://maven-badges.herokuapp.com/maven-central/com.anaptecs.jeaf.core/jeaf-core-impl-project/badge.svg) | <br><br>| [Maven Repo](https://search.maven.org/artifact/com.anaptecs.jeaf.core/jeaf-core-impl-project) | [Git Repo](https://bitbucket.org/anaptecs/jeaf-core-impl/) |
