# JEAF Maven Plugin Latest Versions

|                         |                           |                         |                           |                           |
|-------------------------|:-------------------------:|-------------------------|:-------------------------:|:-------------------------:|
| JEAF Maven Plugin |![Current Version](https://maven-badges.herokuapp.com/maven-central/com.anaptecs.jeaf.maven/jeaf-maven-plugin/badge.svg) | [Plugin Documentation](https://anaptecs.atlassian.net/wiki/spaces/JEAF/pages/559382624) <br>[Release Notes](https://anaptecs.atlassian.net/wiki/spaces/JEAF/pages/1173815297) | [Maven Repo](https://search.maven.org/artifact/com.anaptecs.jeaf.tools/jeaf-tools-api) | [Git Repo](https://bitbucket.org/anaptecs/jeaf-maven-plugin/) |
