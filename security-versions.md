# JEAF Security Latest Versions

|                         |                           |                         |                           |                           |
|-------------------------|:-------------------------:|-------------------------|:-------------------------:|:-------------------------:|
| JEAF Security API |![Current Version](https://maven-badges.herokuapp.com/maven-central/com.anaptecs.jeaf.security/jeaf-security-api-project/badge.svg) | [Javadoc JEAF Authorization Service Provider API](https://javadoc.io/doc/com.anaptecs.jeaf.security/jeaf-authorization-service-provider-api/latest/index.html) <br>[Javadoc JEAF Identity Service Provider API](https://javadoc.io/doc/com.anaptecs.jeaf.security/jeaf-identity-service-provider-api/latest/index.html)<br> | [Maven Repo](https://search.maven.org/artifact/com.anaptecs.jeaf.security/jeaf-security-api) | [Git Repo](https://bitbucket.org/anaptecs/jeaf-security-api.git) |
