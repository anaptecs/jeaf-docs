# JEAF Mail Latest Versions

|                         |                           |                         |                           |                           |
|-------------------------|:-------------------------:|-------------------------|:-------------------------:|:-------------------------:|
| JEAF Mail API |![Current Version](https://maven-badges.herokuapp.com/maven-central/com.anaptecs.jeaf.mail/jeaf-mail-api/badge.svg) | [Javadoc](https://javadoc.io/doc/com.anaptecs.jeaf.mail/jeaf-mail-api/latest/index.html) <br><br> | [Maven Repo](https://search.maven.org/artifact/com.anaptecs.jeaf.mail/jeaf-mail-api) | [Git Repo](https://bitbucket.org/anaptecs/jeaf-mail-api/) |
| JEAF Mail Implementation |![Current Version](https://maven-badges.herokuapp.com/maven-central/com.anaptecs.jeaf.mail/jeaf-mail-impl/badge.svg) | [Javadoc](https://javadoc.io/doc/com.anaptecs.jeaf.mail/jeaf-mail-impl/latest/index.html) <br><br> | [Maven Repo](https://search.maven.org/artifact/com.anaptecs.jeaf.mail/jeaf-mail-impl) | [Git Repo](https://bitbucket.org/anaptecs/jeaf-mail-impl.git) |
