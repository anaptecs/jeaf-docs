# JEAF JSON Latest Versions

|                         |                           |                         |                           |                           |
|-------------------------|:-------------------------:|-------------------------|:-------------------------:|:-------------------------:|
| JEAF JSON API |![Current Version](https://maven-badges.herokuapp.com/maven-central/com.anaptecs.jeaf.json/jeaf-json-api/badge.svg) | [Javadoc](https://javadoc.io/doc/com.anaptecs.jeaf.json/jeaf-json-api) <br><br> | [Maven Repo](https://search.maven.org/artifact/com.anaptecs.jeaf.json/jeaf-x-json-api) | [Git Repo](https://bitbucket.org/anaptecs/jeaf-json-api) |
| JEAF JSON Implementation |![Current Version](https://maven-badges.herokuapp.com/maven-central/com.anaptecs.jeaf.json/jeaf-json-impl/badge.svg) | [Javadoc](https://javadoc.io/doc/com.anaptecs.jeaf.json/jeaf-json-impl/latest/index.html) <br><br> | [Maven Repo](https://search.maven.org/artifact/com.anaptecs.jeaf.json/jeaf-x-json-impl) | [Git Repo](https://bitbucket.org/anaptecs/jeaf-json-impl) |
